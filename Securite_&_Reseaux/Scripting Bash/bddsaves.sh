#! /bin/sh

## récupère le fichier backupconf
source $HOME/.backupconf

#execution du logfile les sorties sont dans les logs
exec >> $logfile 2>&1

## initialisation des variables
savedate=$(date '+%Y-%m-%d_%H-%M-%S')   		
##Année, Mois, Jour, Heure, Minute, Seconde
nbSave=$(ls $backupPath/back*.sql | wc -l)		
##nombre de sauvegarde (compte tous les fichiers commençant par back et en .sql)

if [[ -z $user ]]; 		
	then
	user=$(echo $USER)
fi

## garder les 5 dernières sauvegardes
if [[ $nbSave -ge $maxSave ]]; 					
##nombre de sauvegarde >= au maximum de sauvegarde
	then
	nbDel=$(expr $nbSave - $maxSave + 1)		
	listDel=$(ls $backupPath/back*.sql | sort | head -$nbDel)  	
	##trier les fichiers backup.sql par ordre alphabétique et supprime le premier de la liste
	for save in $listDel										
	do
		rm $backupPath/$save
	done
fi

## lancement du backup
if [[ $databases != "" ]]
then
	mysqldump -u $user -p$secret --dump-date --databases $databases > $backupPath/backupbdd_$savedate.sql
	echo "$(date '+%Y-%m-%d_%H-%M-%S') : Une sauvegarde de $databases a été faite dans $backupPath" >&1

else
	mysqldump -u $user -p$secret --all-databases --dump-date > $backupPath/backupbdd_$savedate.sql
	echo "$(date '+%Y-%m-%d_%H-%M-%S') : Une sauvegarde de toutes les bases de données ont été faites dans $backupPath" >&1
fi
