 # Elastic Search Installation

Elastic Search est d'une puissance remarquable, gestion des logs, analyse des erreurs sans compter scompter le machine learning.
Le projet sera fait sur une centos7.

## Création d'un utilisateur

Il va nous falloir un utilisateur avec les droits sudo pour pouvoir manipuler ElasticSearch.

**adduser [user]**    
**passwd [user]**    
**usermod -a -G wheel [user]**      

Une fois fini, on se connecte au nouvel utilisateur:

**su [user]**

## Mise à jour et Installer Java

Il faut faire une mise à jour au cas où notre serveur centos7 n'est plus à jour.

**sudo yum update**   
**yum install wget**    

Il faut ensuite installer java 1.8 pour pouvoir utiliser Elastic Search

**sudo yum install java-1.8.0-openjdk.x86_64**     
**java -version**

## Installer et configurer ElasticSearch

ElasticSearch a besoin de plusieurs noeuds pour pouvoir fonctionner correctement:     
Node1: 192.168.10.10 (master)    
Node2: 192.168.10.11 (data)    
Node3: 192.168.10.12 (client)    
Pour modifier le fichier de conf réseau:     
**vi /etc/sysconfig/network-scripts/ifcfg-enp0s8**    
**ifdown ifcfg-enp0s8**    
**ifup ifcfg-enp0s8**

Installer:

**wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.noarch.rpm**    
**sudo rpm -ivh elasticsearch-1.7.3.noarch.rpm**  
**sudo systemctl enable elasticsearch.service**   

Configuration:

**sudo vi /etc/elasticsearch/elasticsearch.yml**     

**node.name: "[nom]"**
**cluster.name: ElastikSearch**

Demarer ElasticSearch:
**sudo systemctl start elasticsearch.service**

On peut vérifier que la commande fonctionne grâce à:
**curl -X GET 'http://localhost:9200'**     
**curl -X GET "localhost:9200/_cat/health?v&pretty"**   